package tocomfome;

public class Empregado extends Pessoa{
	private String login;
	private String senha;
	private Pedido pedido;
	private Cliente cliente;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Empregado(String login, String senha) {
		this.login = login;
		this.senha = senha;
	}
	
}
