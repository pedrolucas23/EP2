package tocomfome;

public class Cliente extends Pessoa {
	private String exigencia;

	public String getExigencia() {
		return exigencia;
	}

	public void setExigencia(String exigencia) {
		this.exigencia = exigencia;
	}

	public Cliente(String exigencia) {
		this.exigencia = exigencia;
	}
	
}
