# Universidade de Brasília - Campus Gama
## Orientação a Objetos - Exercício de Programação 2

[1. Fundamentação Teórica] (#1-fundamentação-teórica)

[2. Contexto] (#2-contexto)

[3. Critérios de Avaliação](#3-critérios-de-avaliação)

--------------------------------------------------------------------------

### 1. Fundamentação Teórica

Embora nos últimos anos o desenvolvimento de aplicações desktop esteja diminuindo, ainda há lugares, como na indústria por exemplo, onde este tipo de aplicação ainda é solicitada. Sendo assim é essencial para os desenvolvedores terem pelo menos uma base sobre como desenvolver uma aplicação desktop, tendo em vista uma solução para um possível problema encontrado no mercado.

### 2. Contexto

#### 2.1 Descrição do problema
Você é um desenvolvedor freelancer que aceitou uma proposta para desenvolver uma aplicação desktop para o restaurante “Tô com fome, quero mais”, um restaurante simples gerenciado por uma família.

Para a aplicação desejada pelos donos restaurante, seguem algumas exigências:

* A aplicação será utilizada por um funcionário;
* O restaurante aceita pagamentos a vista e no cartão de crédito;
* O cliente não deverá ter um processo rigoroso de cadastro, de preferencia ele nem deve perceber que fez um cadastro;
* O restaurante disponibiliza vários produtos(pratos, bebidas, sobremesas e etc);
* Os produtos possuem um estoque;
* Se o estoque de um produto ficar abaixo de uma certa quantidade, ele não poderá ser mais oferecido e um alerta deverá surgir na tela do funcionário indicando que o estoque está baixo;
* Um cliente ao fazer um pedido poderá deixar observações quanto ao seu pedido;

Para complicar a sua vida, anteriormente o restaurante já havia encomendado o software para um outro desenvolvedor “mais barato” e desqualificado. Este acabou por fazer uma modelagem incompleta e cabe a você corrigir o que achar necessário(sem comprometer as exigências dos donos do restaurante) e adicionar mais funcionalidades além, é claro, de desenvolver a aplicação.

#### 2.2 Modelagem incompleta do desenvolvedor anterior

![EP2](/uploads/48f73d92f533e3c8e0f36772694fd749/EP2.jpeg)


#### 2.3 Limitações

* A modelagem está incompleta e com erros, você tem liberdade de alterá-la, desde que não fuja do contexto e das exigências do cliente.;
* A modelagem deve estar na raiz da aplicação no formato jpg ou png;
* Você não é obrigado a modelar em inglês como o desenvolvedor anterior;
* Você também não é obrigado a programar em inglês.

### 3. Critérios de Avaliação

| Item | Comentário | Valor  |
|-------|-------------------|---------| 
|Modelagem| Correção da modelagem dada, e utilização correta da UML. | 2,5 |
|Criação das classes da modelagem| Fazer uso correto dos conceitos de OO. | 1,0 |
|Java Swing| Criação de interface gráfica coerente com o contexto dado. | 2,5|
|Tratamento de exceções| Utilização de tratamento de exceções. | 0,5 |
|Uso de pacotes| Manter o projeto organizado, distribuindo as classes em pacotes. | 0,5 |
|Boas práticas de desenvolvimento| Uso de bons nomes, comentários quando necessário, organização das classes e métodos .| 0,5 |
|Testes unitários| Fazer testes unitários, não é necessário testar a interface gráfica. | 2,0 |
|Uso do repositório e Versionamento| Os commits devem estar claros, consistentes e atômicos.  A frequência dos commits deve estar bem distribuída ao longo do tempo. O código deve estar disponível no repositório remoto.  |0,5|
| Uso de Threads [extra]  | Identificar partes do sistema onde podem ser aplicadas threads. | 1,0  |
 

