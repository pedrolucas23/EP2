#    Orientação a Objetos 2/2016 (UnB - Gama)

##   EP2

###  Aluno: Pedro Lucas Pinheiro de Souza
###  Matrícula: 13/0035581

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a tecnologia Java Swing.

#### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Com a IDE NetBeans, aperte:
	**CTRL + SHIFT + O**
* Encontre o diretório raiz do projeto;
* Abra com a IDE;
* Após aberto, aperte:
	**F6**

